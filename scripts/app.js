document.addEventListener("DOMContentLoaded", function(){
    const list = [];
    const form = document.querySelector(".form");
    const items = document.querySelector(".bottom-view-data-box");
    const inputImage = document.querySelector(".input-image");
    const inputTitle = document.querySelector(".input-title");
    const inputDescription = document.querySelector(".input-description");

    form.addEventListener("submit", addItemToList);

    function addItemToList(e) {
        e.preventDefault();

        const title = e.target["input-title"].value;
        const description = e.target["input-description"].value;
        const image = e.target["input-image"].value;

        if(title != "") {
            const item = {
                title: title,
                description: description,
                image: image
            };

            list.push(item);
            renderListItems();
            resetInputs();
        }
    }

    function renderListItems() {
        let itemsStructure = "";

        list.forEach(function(item){
            itemsStructure += `
            <div class="bottom-view-data-card">
                <div class="bottom-view-data-card-image">
                    <img src="${item.image}" alt="" class="img">
                </div>  
                  
                <div class="bottom-view-data-card-texts">
                    <h1>${item.title}</h1>
                    <span>${item.description}</span>
                </div>
            </div>
            `
        });

        items.innerHTML = itemsStructure;
    }

    function resetInputs() {
        inputImage.value = "";
        inputTitle.value = "";
        inputDescription.value = "";
    }
    console.log(resetInputs());
});